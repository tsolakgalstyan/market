﻿using System.ComponentModel.DataAnnotations;
using Market.DAL.Helpers;

namespace Market.Presentation.API.Models
{
	public class UserLogin
	{
		private string _password;

		[Required]
		public string Login { get; set; }

		[Required]
		public string Password
		{
			get => _password;
			set => _password = PasswordHashing.CreateHash(value);
		}
    }
}

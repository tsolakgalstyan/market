﻿using System.Threading.Tasks;
using Market.DAL.Helpers;
using Microsoft.AspNetCore.Mvc;
using Market.DAL.Migrations;
using Market.Presentation.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Market.Presentation.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AuthorizationController : ControllerBase
	{
		private readonly MarketContext _context;

		private static IConfiguration _configuration;

		public AuthorizationController(MarketContext marketContext, IConfiguration configuration)
		{
			_context = marketContext;
			_configuration = configuration;
		}

		[HttpPost("EmployeeLogin")]
		public async Task<ActionResult> EmployeeLogin([FromBody] UserLogin user, [FromServices] Authorize authorizeUser)
		{
			var employeeEntity = await _context.Employees.FirstOrDefaultAsync(u => string.Equals(u.UserName, user.Login));

			if (employeeEntity == null)
			{
				return Ok(new
				{
					Success = false,
					Message = "Employee not found."
				});
			}

			if (!string.Equals(user.Password, employeeEntity.Password)) return Unauthorized();

			var response = await authorizeUser.GetJwtStringAsync(employeeEntity.UserName, "Employee");
			return Ok(response);

		}

		[HttpPost("CustomerLogin")]
		public async Task<ActionResult> CustomerLogin([FromBody] UserLogin user, [FromServices] Authorize authorizeUser)
		{
			var customerEntity = await _context.Customers.FirstOrDefaultAsync(u => string.Equals(u.UserName, user.Login));

			if (customerEntity == null)
			{
				return Ok(new
				{
					Success = false,
					Message = "Customer not found."
				});
			}

			if (!string.Equals(user.Password, customerEntity.Password)) return Unauthorized();

			var response = await authorizeUser.GetJwtStringAsync(customerEntity.UserName, "Customer");
			return Ok(response);

		}
	}
}

﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Market.DAL.Helpers;
using Market.DAL.Migrations;
using Market.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Market.Presentation.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
#if !DEBUG
	[Authorize(Roles = "Employee")]
#endif
	public class CustomersController : ControllerBase
	{
		private readonly MarketContext _context;

		public CustomersController(MarketContext context)
		{
			_context = context;
		}

		// GET: api/<CustomersController>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<Customer>>> Get()
		{
			return await _context.Customers.ToListAsync();
		}

		// GET api/<CustomersController>/5
		[HttpGet("{id}")]
		public async Task<ActionResult<Customer>> Get(int id)
		{
			var customer = await _context.Customers.FindAsync(id);
			if (customer == null)
			{
				return Ok(new
				{
					Success = false,
					Message = "Customer not Found."
				});
			}
			return customer;
		}

		// POST api/<CustomersController>
		[HttpPost]
		public async Task<ActionResult<Customer>> Post([FromBody] Customer customer)
		{
			_context.Add(customer);
			await _context.SaveChangesAsync();

			return CreatedAtAction("Get", new { id = customer.CustomerId }, customer);
		}

		// PUT api/<CustomersController>/5
		[HttpPut("{id}")]
		public async Task<IActionResult> Put(int id, [FromBody] Customer customer)
		{
			if (id != customer.CustomerId)
			{
				return BadRequest();
			}

			var customerPassword = await GetCustomerPass(id);
			if (!string.Equals(customer.Password, customerPassword))
			{
				customer.Password = PasswordHashing.CreateHash(customer.Password);
			}

			_context.Entry(customer).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!_context.Customers.Any(e => e.CustomerId == id))
				{
					return Ok(new
					{
						Success = false,
						Message = "Customer not found."
					});
				}
			}

			return NoContent();
		}

		// DELETE api/<CustomersController>/5
		[HttpDelete("{id}")]
		public async Task<ActionResult<Customer>> Delete(int id)
		{
			var customer = await _context.Customers.FindAsync(id);
			if (customer == null)
			{
				return NotFound(new
				{
					Success = false,
					Message = "Customer not found."
				});
			}

			_context.Customers.Remove(customer);
			await _context.SaveChangesAsync();

			return customer;
		}

		private async Task<string> GetCustomerPass(int id)
		{
			var customerEntity = await _context.Customers.FindAsync(id);
			var result = customerEntity.Password;
			_context.Entry(customerEntity).State = EntityState.Detached;

			return result;
		}
	}
}

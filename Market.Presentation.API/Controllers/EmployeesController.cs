﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Market.DAL.Helpers;
using Market.DAL.Migrations;
using Market.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Market.Presentation.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
#if !DEBUG
[Authorize(Roles = "Employee")]
#endif
	public class EmployeesController : ControllerBase
	{
		private readonly MarketContext _context;

		public EmployeesController(MarketContext context)
		{
			_context = context;
		}

		// GET: api/<CustomersController>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<Employee>>> Get()
		{
			return await _context.Employees.ToListAsync();
		}

		// GET api/<CustomersController>/5
		[HttpGet("{id}")]
		public async Task<ActionResult<Employee>> Get(int id)
		{
			var employee = await _context.Employees.FindAsync(id);
			if (employee == null)
			{
				return Ok(new
				{
					Success = false,
					Message = "Employee not Found."
				});
			}
			return employee;
		}

		// POST api/<CustomersController>
		[HttpPost]
		public async Task<ActionResult<Employee>> Post([FromBody] Employee employee)
		{
			try
			{
				employee.Password = PasswordHashing.CreateHash(employee.Password);
				_context.Add(employee);
				await _context.SaveChangesAsync();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}


			return CreatedAtAction("Get", new { id = employee.EmployeeId }, employee);
		}

		// PUT api/<EmployeesController>/5
		[HttpPut("{id}")]
		public async Task<IActionResult> Put(int id, [FromBody] Employee employee)
		{
			if (id != employee.EmployeeId)
			{
				return BadRequest();
			}

			var employeePassword = await GetEmployeePass(id);
			if (!string.Equals(employee.Password, employeePassword))
			{
				employee.Password = PasswordHashing.CreateHash(employee.Password);
			}

			_context.Entry(employee).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!_context.Employees.Any(e => e.EmployeeId == id))
				{
					return Ok(new
					{
						Success = false,
						Message = "Employee not found."
					});
				}
			}

			return NoContent();
		}

		// DELETE api/<EmployeesController>/5
		[HttpDelete("{id}")]
		public async Task<ActionResult<Employee>> Delete(int id)
		{
			var employee = await _context.Employees.FindAsync(id);
			if (employee == null)
			{
				return NotFound(new
				{
					Success = false,
					Message = "Employee not found."
				});
			}

			_context.Employees.Remove(employee);
			await _context.SaveChangesAsync();

			return employee;
		}

		private async Task<string> GetEmployeePass(int id)
		{
			var employeeEntity = await _context.Employees.FindAsync(id);
			var result = employeeEntity.Password;
			_context.Entry(employeeEntity).State = EntityState.Detached;

			return result;
		}
	}
}

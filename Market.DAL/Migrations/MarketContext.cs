﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Market.DAL.Models;
using System.Threading.Tasks;
using System.Threading;

#nullable disable

namespace Market.DAL.Migrations
{
    public partial class MarketContext : DbContext
    {
        public MarketContext()
        {
        }

        public MarketContext(DbContextOptions<MarketContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DigitalReceipt> DigitalReceipts { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Inventory> Inventories { get; set; }
        public virtual DbSet<LoyaltyDiscount> LoyaltyDiscounts { get; set; }
        public virtual DbSet<MeasurementUnit> MeasurementUnits { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductPricePerWeight> ProductPricePerWeights { get; set; }
        public virtual DbSet<Return> Returns { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer");

                entity.HasComment("Basic information \r\nabout Customer");

                entity.HasIndex(e => e.FirstName, "AK1_Customer_CustomerName")
                    .IsUnique();

                entity.HasIndex(e => e.LoyaltyDiscountId, "fkIdx_262");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.LoyaltyDiscount)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.LoyaltyDiscountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_261");
            });

            modelBuilder.Entity<DigitalReceipt>(entity =>
            {
                entity.ToTable("DigitalReceipt");

                entity.HasIndex(e => e.OrderId, "fkIdx_274");

                entity.HasIndex(e => e.EmployeeId, "fkIdx_281");

                entity.HasIndex(e => e.ReturnsId, "fkIdx_291");

                entity.Property(e => e.DigitalReceiptId).ValueGeneratedNever();

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.DigitalReceipts)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_280");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.DigitalReceipts)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_273");

                entity.HasOne(d => d.Returns)
                    .WithMany(p => p.DigitalReceipts)
                    .HasForeignKey(d => d.ReturnsId)
                    .HasConstraintName("FK_290");
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.ToTable("Discount");

                entity.HasIndex(e => e.EmployeeId, "fkIdx_208");

                entity.Property(e => e.DiscountId).ValueGeneratedNever();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Discounts)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_207");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.HasComment("Basic information \r\nabout Customer");

                entity.HasIndex(e => e.FirstName, "AK1_Customer_CustomerName_clone")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Inventory>(entity =>
            {
                entity.ToTable("Inventory");

                entity.HasComment("Basic information \r\nabout Customer");

                entity.HasIndex(e => e.InventoryName, "AK1_Customer_CustomerName_clone_clone_1")
                    .IsUnique();

                entity.HasIndex(e => e.EmployeeId, "fkIdx_178");

                entity.HasIndex(e => e.ProductId, "fkIdx_185");

                entity.HasIndex(e => e.SupplierId, "fkIdx_198");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.InventoryName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.Quantity).HasColumnType("decimal(12, 2)");

                entity.Property(e => e.SuppliedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Inventories)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_177");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Inventories)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_184");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Inventories)
                    .HasForeignKey(d => d.SupplierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_197");
            });

            modelBuilder.Entity<LoyaltyDiscount>(entity =>
            {
                entity.ToTable("LoyaltyDiscount");

                entity.HasIndex(e => e.DiscountId, "fkIdx_259");

                entity.Property(e => e.LoyaltyDiscountId).ValueGeneratedNever();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.LoyaltyDiscountByAmount).HasColumnType("decimal(12, 2)");

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.HasOne(d => d.Discount)
                    .WithMany(p => p.LoyaltyDiscounts)
                    .HasForeignKey(d => d.DiscountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_258");
            });

            modelBuilder.Entity<MeasurementUnit>(entity =>
            {
                entity.HasKey(e => e.UnitId)
                    .HasName("PK_Customer_clone_clone_clone");

                entity.ToTable("MeasurementUnit");

                entity.HasComment("Basic information \r\nabout Customer");

                entity.HasIndex(e => e.UnitName, "AK1_Customer_CustomerName_clone_clone_clone")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.UnitName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("Order");

                entity.HasComment("Order information\r\nlike Date, Amount");

                entity.HasIndex(e => e.OrderNumber, "AK1_Order_OrderNumber")
                    .IsUnique();

                entity.HasIndex(e => e.CustomerId, "FK_Order_CustomerId_Customer");

                entity.HasIndex(e => e.ProductId, "fkIdx_219");

                entity.Property(e => e.OrderDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrderNumber).HasMaxLength(10);

                entity.Property(e => e.Quantity).HasColumnType("decimal(12, 2)");

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(12, 2)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Order_CustomerId_Customer");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_218");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.HasComment("Basic information \r\nabout Customer");

                entity.HasIndex(e => e.ProductName, "AK1_Customer_CustomerName_clone_clone")
                    .IsUnique();

                entity.HasIndex(e => e.UnitId, "fkIdx_161");

                entity.HasIndex(e => e.EmployeeId, "fkIdx_168");

                entity.HasIndex(e => e.DiscountId, "fkIdx_213");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(12, 2)");

                entity.HasOne(d => d.Discount)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.DiscountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_212");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_167");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.UnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_160");
            });

            modelBuilder.Entity<ProductPricePerWeight>(entity =>
            {
                entity.ToTable("ProductPricePerWeight");

                entity.HasIndex(e => e.ProductId, "fkIdx_235");

                entity.HasIndex(e => e.EmployeeId, "fkIdx_238");

                entity.Property(e => e.ProductPricePerWeightId).ValueGeneratedNever();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("decimal(12, 2)");

                entity.Property(e => e.Weight).HasColumnType("decimal(12, 2)");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.ProductPricePerWeights)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_237");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductPricePerWeights)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_234");
            });

            modelBuilder.Entity<Return>(entity =>
            {
                entity.HasKey(e => e.ReturnsId)
                    .HasName("PK_returns");

                entity.HasIndex(e => e.OrderId, "fkIdx_287");

                entity.Property(e => e.ReturnsId).ValueGeneratedNever();

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Returns)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_286");
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("Supplier");

                entity.HasComment("Basic information \r\nabout Supplier");

                entity.HasIndex(e => e.CompanyName, "AK1_Supplier_CompanyName")
                    .IsUnique();

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
	        try
	        {
		        var insertedEntries = ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToList();

		        insertedEntries.ForEach(e =>
		        {
			        e.Property("Created").CurrentValue = DateTime.Now;
		        });

		        var updatedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified).ToList();

		        updatedEntities.ForEach(e =>
		        {
			        e.Property("Modified").CurrentValue = DateTime.Now;
		        });
	        }
	        catch (Exception)
	        {
		        //throw;
	        }

	        return base.SaveChangesAsync(cancellationToken);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

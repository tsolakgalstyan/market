﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Market.DAL.Helpers
{
	public class Authorize
	{
		private static IConfiguration _configuration;

		public Authorize(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public async Task<string> GetJwtStringAsync(string login, string role, int tokenExpireTimeInHours = 24)
		{
			return await Task.Run(() =>
			{
				var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSecretKey"]));
				var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

				var claims = new List<Claim>
				{
					new Claim(ClaimTypes.Name, login),
					new Claim(ClaimTypes.Role, role)
				};

				DateTime? expire = null;
				if (tokenExpireTimeInHours != 0)
				{
					expire = DateTime.Now.AddHours(tokenExpireTimeInHours);
				}

				var tokenOptions = new JwtSecurityToken
				(
					claims: claims,
					expires: expire,
					signingCredentials: signinCredentials
				);
				return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
			});
		}
	}
}

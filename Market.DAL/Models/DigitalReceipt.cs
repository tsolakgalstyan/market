﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class DigitalReceipt
    {
        public int DigitalReceiptId { get; set; }
        public int OrderId { get; set; }
        public int EmployeeId { get; set; }
        public int? ReturnsId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Order Order { get; set; }
        public virtual Return Returns { get; set; }
    }
}

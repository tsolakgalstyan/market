﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
            Inventories = new HashSet<Inventory>();
        }

        public int SupplierId { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Inventory> Inventories { get; set; }
    }
}

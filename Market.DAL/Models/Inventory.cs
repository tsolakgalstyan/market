﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Inventory
    {
        public int InventoryId { get; set; }
        public string InventoryName { get; set; }
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public int EmployeeId { get; set; }
        public int SupplierId { get; set; }
        public DateTime SuppliedDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Product Product { get; set; }
        public virtual Supplier Supplier { get; set; }
    }
}

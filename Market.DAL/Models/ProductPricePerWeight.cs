﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class ProductPricePerWeight
    {
        public int ProductPricePerWeightId { get; set; }
        public int EmployeeId { get; set; }
        public int ProductId { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Product Product { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Employee
    {
	    public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? LastLogin { get; set; }

        [JsonIgnore]
        public ICollection<DigitalReceipt> DigitalReceipts { get; set; }
        [JsonIgnore]
        public ICollection<Discount> Discounts { get; set; }
        [JsonIgnore]
        public ICollection<Inventory> Inventories { get; set; }
        [JsonIgnore]
        public ICollection<ProductPricePerWeight> ProductPricePerWeights { get; set; }
        [JsonIgnore]
        public ICollection<Product> Products { get; set; }
    }
}

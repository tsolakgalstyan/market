﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Return
    {
        public Return()
        {
            DigitalReceipts = new HashSet<DigitalReceipt>();
        }

        public int ReturnsId { get; set; }
        public int OrderId { get; set; }
        public DateTime ReturnDate { get; set; }

        public virtual Order Order { get; set; }
        public virtual ICollection<DigitalReceipt> DigitalReceipts { get; set; }
    }
}

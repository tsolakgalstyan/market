﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Order
    {
        public Order()
        {
            DigitalReceipts = new HashSet<DigitalReceipt>();
            Returns = new HashSet<Return>();
        }

        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime OrderDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<DigitalReceipt> DigitalReceipts { get; set; }
        public virtual ICollection<Return> Returns { get; set; }
    }
}

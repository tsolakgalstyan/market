﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Discount
    {
        public Discount()
        {
            LoyaltyDiscounts = new HashSet<LoyaltyDiscount>();
            Products = new HashSet<Product>();
        }

        public int DiscountId { get; set; }
        public int? DiscountPercent { get; set; }
        public int? DiscountAmount { get; set; }
        public int EmployeeId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual ICollection<LoyaltyDiscount> LoyaltyDiscounts { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class LoyaltyDiscount
    {
        public LoyaltyDiscount()
        {
            Customers = new HashSet<Customer>();
        }

        public int LoyaltyDiscountId { get; set; }
        public int DiscountId { get; set; }
        public int? LoyaltyDiscountByOrder { get; set; }
        public decimal? LoyaltyDiscountByAmount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public virtual Discount Discount { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}

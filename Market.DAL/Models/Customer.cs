﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Orders = new HashSet<Order>();
        }

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public int? TotalOrdersCount { get; set; }
        public int? TotalBoughtAmount { get; set; }
        public int? LoyaltyDiscountId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime LastLogin { get; set; }

        [JsonIgnore]
        public virtual LoyaltyDiscount LoyaltyDiscount { get; set; }
        [JsonIgnore]
        public virtual ICollection<Order> Orders { get; set; }
    }
}

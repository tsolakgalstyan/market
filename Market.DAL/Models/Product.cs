﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Market.DAL.Models
{
    public partial class Product
    {
        public Product()
        {
            Inventories = new HashSet<Inventory>();
            Orders = new HashSet<Order>();
            ProductPricePerWeights = new HashSet<ProductPricePerWeight>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int UnitId { get; set; }
        public decimal UnitPrice { get; set; }
        public int? DailyDiscount { get; set; }
        public int EmployeeId { get; set; }
        public int DiscountId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public virtual Discount Discount { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual MeasurementUnit Unit { get; set; }
        public virtual ICollection<Inventory> Inventories { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<ProductPricePerWeight> ProductPricePerWeights { get; set; }
    }
}
